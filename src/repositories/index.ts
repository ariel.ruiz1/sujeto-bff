export * from './sujeto.repository';
export * from './obligacion.repository';
export * from './objeto.repository';
export * from './domicilio-objeto.repository';
export * from './domicilio-sujeto.repository';
