import { DefaultCrudRepository, repository, BelongsToAccessor } from '@loopback/repository';
import { Obligacion, ObligacionRelations, Objeto } from '../models';
import { CassandraBucDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class ObligacionRepository extends DefaultCrudRepository<
  Obligacion,
  typeof Obligacion.prototype.bob_obn_id,
  ObligacionRelations
  > {

  constructor(
    @inject('datasources.cassandra_buc') dataSource: CassandraBucDataSource,
  ) {
    super(Obligacion, dataSource);
  }
}
