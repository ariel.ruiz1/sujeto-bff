import { DefaultCrudRepository, repository, HasManyRepositoryFactory } from '@loopback/repository';
import { Objeto, ObjetoRelations, Obligacion, DomicilioObjeto} from '../models';
import { CassandraBucDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { ObligacionRepository } from './obligacion.repository';
import {DomicilioObjetoRepository} from './domicilio-objeto.repository';

export class ObjetoRepository extends DefaultCrudRepository<
  Objeto,
  typeof Objeto.prototype.soj_identificador,
  ObjetoRelations
  > {

  public readonly obligaciones: HasManyRepositoryFactory<Obligacion, typeof Objeto.prototype.soj_identificador>;

  public readonly domicilioObjetos: HasManyRepositoryFactory<DomicilioObjeto, typeof Objeto.prototype.soj_identificador>;

  constructor(
    @inject('datasources.cassandra_buc') dataSource: CassandraBucDataSource, @repository.getter('ObligacionRepository') protected obligacionRepositoryGetter: Getter<ObligacionRepository>, @repository.getter('DomicilioObjetoRepository') protected domicilioObjetoRepositoryGetter: Getter<DomicilioObjetoRepository>,
  ) {
    super(Objeto, dataSource);
    this.domicilioObjetos = this.createHasManyRepositoryFactoryFor('domicilioObjetos', domicilioObjetoRepositoryGetter,);
    this.obligaciones = this.createHasManyRepositoryFactoryFor('obligaciones', obligacionRepositoryGetter);
  }
}
