import { DefaultCrudRepository, repository, HasManyRepositoryFactory } from '@loopback/repository';
import { Sujeto, SujetoRelations, Objeto, Obligacion, DomicilioSujeto} from '../models';
import { CassandraBucDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { ObjetoRepository } from './objeto.repository';
import { ObligacionRepository } from './obligacion.repository';
import {DomicilioSujetoRepository} from './domicilio-sujeto.repository';

export class SujetoRepository extends DefaultCrudRepository<
  Sujeto,
  typeof Sujeto.prototype.suj_identificador,
  SujetoRelations
  > {

  public readonly objetos: HasManyRepositoryFactory<Objeto, typeof Sujeto.prototype.suj_identificador>;

  public readonly obligaciones: HasManyRepositoryFactory<Obligacion, typeof Sujeto.prototype.suj_identificador>;

  public readonly domicilioSujetos: HasManyRepositoryFactory<DomicilioSujeto, typeof Sujeto.prototype.suj_identificador>;

  constructor(
    @inject('datasources.cassandra_buc') dataSource: CassandraBucDataSource, @repository.getter('ObjetoRepository') protected objetoRepositoryGetter: Getter<ObjetoRepository>, @repository.getter('ObligacionRepository') protected obligacionRepositoryGetter: Getter<ObligacionRepository>, @repository.getter('DomicilioSujetoRepository') protected domicilioSujetoRepositoryGetter: Getter<DomicilioSujetoRepository>,
  ) {
    super(Sujeto, dataSource);
    this.domicilioSujetos = this.createHasManyRepositoryFactoryFor('domicilioSujetos', domicilioSujetoRepositoryGetter,);
    this.obligaciones = this.createHasManyRepositoryFactoryFor('obligaciones', obligacionRepositoryGetter);
    this.objetos = this.createHasManyRepositoryFactoryFor('objetos', objetoRepositoryGetter);
  }
}
