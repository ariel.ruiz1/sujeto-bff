export * from './sujeto.model';
export * from './obligacion.model';
export * from './objeto.model';
export * from './domicilio-sujeto.model';
export * from './domicilio-objeto.model';
