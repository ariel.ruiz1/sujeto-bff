import { Entity, model, property } from '@loopback/repository';

@model({ name: 'buc_domicilios_sujeto' })
export class DomicilioSujeto extends Entity {
  @property({
    type: 'string',
    id: true,
    required: true,
    generated: false,
  })
  bds_dom_id: string;

  @property({
    type: 'string',
  })
  bds_suj_identificador?: string;

  @property({
    type: 'string',
  })
  bds_barrio?: string;

  @property({
    type: 'string',
  })
  bds_calle?: string;

  @property({
    type: 'string',
  })
  bds_codigo_postal?: string;

  @property({
    type: 'string',
  })
  bds_dpto?: string;

  @property({
    type: 'string',
  })
  bds_estado?: string;

  @property({
    type: 'string',
  })
  bds_kilometro?: string;

  @property({
    type: 'string',
  })
  bds_localidad?: string;

  @property({
    type: 'string',
  })
  bds_lote?: string;

  @property({
    type: 'string',
  })
  bds_manzana?: string;

  @property({
    type: 'string',
  })
  bds_piso?: string;

  @property({
    type: 'string',
  })
  bds_provincia?: string;

  @property({
    type: 'string',
  })
  bds_puerta?: string;

  @property({
    type: 'string',
  })
  bds_tipo?: string;

  @property({
    type: 'string',
  })
  bds_torre?: string;


  constructor(data?: Partial<DomicilioSujeto>) {
    super(data);
  }
}

export interface DomicilioSujetoRelations {
  // describe navigational properties here
}

export type DomicilioSujetoWithRelations = DomicilioSujeto & DomicilioSujetoRelations;
