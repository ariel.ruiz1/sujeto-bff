import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Objeto } from './objeto.model';

@model({ name: 'buc_obligaciones', settings: { strict: false } })
export class Obligacion extends Entity {
  @property({
    type: 'string',
    id: true,
    required: true,
    generated: false,
  })
  bob_obn_id: string;

  @property({
    type: 'string',
  })
  bob_suj_identificador?: string;

  @property({
    type: 'string',
  })
  bob_soj_tipo_objeto?: string;

  @property({
    type: 'string',
  })
  bob_soj_identificador?: string;

  @property({
    type: 'number',
  })
  bob_capital?: number;

  @property({
    type: 'string',
  })
  bob_cuota?: string;

  @property({
    type: 'string',
  })
  bob_estado?: string;

  @property({
    type: 'string',
  })
  bob_fiscalizada?: string;

  @property({
    type: 'string',
  })
  bob_indice_int_punit?: string;

  @property({
    type: 'string',
  })
  bob_indice_int_resar?: string;

  @property({
    type: 'number',
  })
  bob_interes_punit?: number;

  @property({
    type: 'number',
  })
  bob_interes_resar?: number;

  @property({
    type: 'number',
  })
  bob_jui_id?: number;

  @property({
    type: 'string',
  })
  bob_otros_atributos?: string;

  @property({
    type: 'string',
  })
  bob_periodo?: string;

  @property({
    type: 'number',
  })
  bob_pln_id?: number;

  @property({
    type: 'date',
  })
  bob_prorroga?: string;

  @property({
    type: 'string',
  })
  bob_tipo?: string;

  @property({
    type: 'number',
  })
  bob_total?: number;

  @property({
    type: 'date',
  })
  bob_vencimiento?: string;

  constructor(data?: Partial<Obligacion>) {
    super(data);
  }
}

export interface ObligacionRelations {
  // describe navigational properties here
}

export type ObligacionWithRelations = Obligacion & ObligacionRelations;
