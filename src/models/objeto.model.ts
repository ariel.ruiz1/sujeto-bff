import { Entity, model, property, hasMany } from '@loopback/repository';
import { Obligacion } from './obligacion.model';
import {DomicilioObjeto} from './domicilio-objeto.model';

@model({ name: 'buc_sujeto_objeto' })
export class Objeto extends Entity {
  @property({
    type: 'string',
    id: true,
    required: true,
    generated: false,
  })
  soj_identificador: string;

  @property({
    type: 'string',
  })
  soj_suj_identificador?: string;

  @property({
    type: 'string',
  })
  soj_tipo_objeto?: string;

  @property({
    type: 'number',
  })
  soj_cat_soj_id?: number;

  @property({
    type: 'string',
  })
  soj_descripcion?: string;

  @property({
    type: 'string',
  })
  soj_estado?: string;

  @property({
    type: 'date',
  })
  soj_fecha_fin?: string;

  @property({
    type: 'date',
  })
  soj_fecha_inicio?: string;

  @property({
    type: 'string',
  })
  soj_id_externo?: string;

  @property({
    type: 'string',
  })
  soj_otros_atributos?: string;

  @property({
    type: 'number',
  })
  soj_saldo?: number;

  @hasMany(() => Obligacion ,{keyTo: 'bob_soj_identificador'})
  obligaciones: Obligacion[];

  @hasMany(() => DomicilioObjeto ,{keyTo: 'bdo_soj_identificador'})
  domicilioObjetos: DomicilioObjeto[];

  constructor(data?: Partial<Objeto>) {
    super(data);
  }
}

export interface ObjetoRelations {
  // describe navigational properties here
}

export type ObjetoWithRelations = Objeto & ObjetoRelations;
