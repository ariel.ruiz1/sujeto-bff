import { Entity, model, property, hasMany} from '@loopback/repository';
import {Objeto} from './objeto.model';
import {Obligacion} from './obligacion.model';
import {DomicilioSujeto} from './domicilio-sujeto.model';

@model({ name: 'buc_sujeto', settings: { strict: false } })
export class Sujeto extends Entity {
  @property({
    type: 'string',
    id: true,
    required: true,
    generated: false,
  })
  suj_identificador: string;

  @property({
    type: 'number',
  })
  suj_cat_suj_id?: number;

  @property({
    type: 'string',
  })
  suj_denominacion?: string;

  @property({
    type: 'string',
  })
  suj_dfe?: string;

  @property({
    type: 'string',
  })
  suj_direccion?: string;

  @property({
    type: 'string',
  })
  suj_email?: string;

  @property({
    type: 'string',
  })
  suj_id_externo?: string;

  @property({
    type: 'string',
  })
  suj_otros_atributos?: string;

  @property({
    type: 'string',
  })
  suj_riesgo_fiscal?: string;

  @property({
    type: 'number',
  })
  suj_saldo?: number;

  @property({
    type: 'string',
  })
  suj_situacion_fiscal?: string;

  @property({
    type: 'string',
  })
  suj_telefono?: string;

  @property({
    type: 'string',
  })
  suj_tipo?: string;

  @hasMany(() => Objeto ,{keyTo: 'soj_suj_identificador'})
  objetos: Objeto[];

  @hasMany(() => Obligacion ,{keyTo: 'bob_suj_identificador'})
  obligaciones: Obligacion[];

  @hasMany(() => DomicilioSujeto ,{keyTo: 'bds_suj_identificador'})
  domicilioSujetos: DomicilioSujeto[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Sujeto>) {
    super(data);
  }
}

export interface SujetoRelations {
  // describe navigational properties here
}

export type SujetoWithRelations = Sujeto & SujetoRelations;
