import { Entity, model, property } from '@loopback/repository';

@model({ name: 'buc_domicilios_objeto' })
export class DomicilioObjeto extends Entity {
  @property({
    type: 'string',
    id: true,
    required: true,
    generated: false,
  })
  bdo_dom_id: string;

  @property({
    type: 'string',
  })
  bdo_suj_identificador?: string;

  @property({
    type: 'string',
  })
  bdo_soj_tipo_objeto?: string;

  @property({
    type: 'string',
  })
  bdo_soj_identificador?: string;

  @property({
    type: 'string',
  })
  bdo_barrio?: string;

  @property({
    type: 'string',
  })
  bdo_calle?: string;

  @property({
    type: 'string',
  })
  bdo_codigo_postal?: string;

  @property({
    type: 'string',
  })
  bdo_dpto?: string;

  @property({
    type: 'string',
  })
  bdo_estado?: string;

  @property({
    type: 'string',
  })
  bdo_kilometro?: string;

  @property({
    type: 'string',
  })
  bdo_localidad?: string;

  @property({
    type: 'string',
  })
  bdo_lote?: string;

  @property({
    type: 'string',
  })
  bdo_manzana?: string;

  @property({
    type: 'string',
  })
  bdo_piso?: string;

  @property({
    type: 'string',
  })
  bdo_provincia?: string;

  @property({
    type: 'string',
  })
  bdo_puerta?: string;

  @property({
    type: 'string',
  })
  bdo_tipo?: string;

  @property({
    type: 'string',
  })
  bdo_torre?: string;


  constructor(data?: Partial<DomicilioObjeto>) {
    super(data);
  }
}

export interface DomicilioObjetoRelations {
  // describe navigational properties here
}

export type DomicilioObjetoWithRelations = DomicilioObjeto & DomicilioObjetoRelations;
