import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
} from '@loopback/rest';
import {
  Obligacion,
} from '../models';
import { SujetoRepository } from '../repositories';

export class SujetoObligacionController {
  constructor(
    @repository(SujetoRepository) protected sujetoRepository: SujetoRepository,
  ) { }

  @get('/sujetos/{id}/obligaciones', {
    responses: {
      '200': {
        description: 'Array of Obligacion\'s belonging to Sujeto',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Obligacion) },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Obligacion>,
  ): Promise<Obligacion[]> {
    return this.sujetoRepository.obligaciones(id).find(filter);
  }
}
