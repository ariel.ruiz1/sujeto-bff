import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
} from '@loopback/rest';
import {
  Objeto,
} from '../models';
import { SujetoRepository } from '../repositories';

export class SujetoObjetoController {
  constructor(
    @repository(SujetoRepository) protected sujetoRepository: SujetoRepository,
  ) { }

  @get('/sujetos/{id}/objetos', {
    responses: {
      '200': {
        description: 'Array of Objeto\'s belonging to Sujeto',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Objeto) },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Objeto>,
  ): Promise<Objeto[]> {
    return this.sujetoRepository.objetos(id).find(filter);
  }
}
