import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
} from '@loopback/rest';
import { Sujeto } from '../models';
import { SujetoRepository } from '../repositories';

export class SujetoController {
  constructor(
    @repository(SujetoRepository)
    public SujetoRepository: SujetoRepository,
  ) { }

  @get('/sujetos', {
    responses: {
      '200': {
        description: 'Array of Sujeto model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Sujeto) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Sujeto)) filter?: Filter<Sujeto>,
  ): Promise<Sujeto[]> {
    return this.SujetoRepository.find(filter);
  }

  @get('/sujetos/{id}', {
    responses: {
      '200': {
        description: 'Sujeto model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Sujeto) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Sujeto> {
    return this.SujetoRepository.findById(id);
  }
}
