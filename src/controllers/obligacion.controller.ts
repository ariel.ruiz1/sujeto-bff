import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
} from '@loopback/rest';
import { Obligacion } from '../models';
import { ObligacionRepository } from '../repositories';

export class ObligacionController {
  constructor(
    @repository(ObligacionRepository)
    public obligacionRepository: ObligacionRepository,
  ) { }

  @get('/obligaciones', {
    responses: {
      '200': {
        description: 'Array of Obligacion model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Obligacion) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Obligacion)) filter?: Filter<Obligacion>,
  ): Promise<Obligacion[]> {
    return this.obligacionRepository.find(filter);
  }

  @get('/obligaciones/{id}', {
    responses: {
      '200': {
        description: 'Obligacion model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Obligacion) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Obligacion> {
    return this.obligacionRepository.findById(id);
  }
}
