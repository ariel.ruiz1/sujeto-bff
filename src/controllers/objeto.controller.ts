import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
} from '@loopback/rest';
import { Objeto } from '../models';
import { ObjetoRepository } from '../repositories';

export class ObjetoController {
  constructor(
    @repository(ObjetoRepository)
    public objetoRepository: ObjetoRepository,
  ) { }

  @get('/objetos', {
    responses: {
      '200': {
        description: 'Array of Objeto model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Objeto) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Objeto)) filter?: Filter<Objeto>,
  ): Promise<Objeto[]> {
    return this.objetoRepository.find(filter);
  }

  @get('/objetos/{id}', {
    responses: {
      '200': {
        description: 'Objeto model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Objeto) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Objeto> {
    return this.objetoRepository.findById(id);
  }
}
