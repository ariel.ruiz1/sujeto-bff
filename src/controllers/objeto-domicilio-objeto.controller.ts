import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
} from '@loopback/rest';
import {
  DomicilioObjeto,
} from '../models';
import { ObjetoRepository } from '../repositories';

export class ObjetoDomicilioObjetoController {
  constructor(
    @repository(ObjetoRepository) protected objetoRepository: ObjetoRepository,
  ) { }

  @get('/objetos/{id}/domicilios', {
    responses: {
      '200': {
        description: 'Array of DomicilioObjeto\'s belonging to Objeto',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(DomicilioObjeto) },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<DomicilioObjeto>,
  ): Promise<DomicilioObjeto[]> {
    return this.objetoRepository.domicilioObjetos(id).find(filter);
  }
}
