import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
} from '@loopback/rest';
import {
  DomicilioSujeto,
} from '../models';
import { SujetoRepository } from '../repositories';

export class SujetoDomicilioSujetoController {
  constructor(
    @repository(SujetoRepository) protected sujetoRepository: SujetoRepository,
  ) { }

  @get('/sujetos/{id}/domicilios', {
    responses: {
      '200': {
        description: 'Array of DomicilioSujeto\'s belonging to Sujeto',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(DomicilioSujeto) },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<DomicilioSujeto>,
  ): Promise<DomicilioSujeto[]> {
    return this.sujetoRepository.domicilioSujetos(id).find(filter);
  }
}
