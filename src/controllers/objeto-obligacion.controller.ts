import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Objeto,
  Obligacion,
} from '../models';
import { ObjetoRepository } from '../repositories';

export class ObjetoObligacionController {
  constructor(
    @repository(ObjetoRepository) protected objetoRepository: ObjetoRepository,
  ) { }

  @get('/objetos/{id}/obligaciones', {
    responses: {
      '200': {
        description: 'Array of Obligacion\'s belonging to Objeto',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Obligacion) },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Obligacion>,
  ): Promise<Obligacion[]> {
    return this.objetoRepository.obligaciones(id).find(filter);
  }
}
